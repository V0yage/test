
var RM = window.RM = window.RM || {};

window.RM.config = {
  root: "/commerce/",
  pushState: true
}

window.chunkURL = "/commerce/dist/";
