<?php

namespace classes;
use core\classes\Product;

class Notebook extends Product
{
    private $maker;

    public function __construct($name, $price, $maker)
    {
        parent::__construct($name, $price);
        $this->maker = $maker;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->maker;
    }

    /**
     * @param mixed $maker
     */
    public function setAuthor($maker)
    {
        $this->maker = $maker;
    }

    public function getAboutInfo()
    {
        $aboutInfo = parent::getAboutInfo()
            . "Производитель: {$this->maker}<br>";
        return $aboutInfo;
    }

    public function getType()
    {
        return 'Ноутбук';
    }
}