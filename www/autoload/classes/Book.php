<?php

namespace classes;
use core\classes\Product;

class Book extends Product
{
    private $pages;
    private $author;

    public function __construct($name, $price, $pages, $author)
    {
        parent::__construct($name, $price);
        $this->pages = $pages;
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param mixed $pages
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getAboutInfo()
    {
        $aboutInfo = parent::getAboutInfo()
                    . "Количество страниц: {$this->pages}<br>"
                    . "Автор: {$this->author}<br>";
        return $aboutInfo;
    }

    public function getType()
    {
        return 'Книга';
    }
}