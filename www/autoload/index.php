<?php

error_reporting(E_ALL);

use classes\Book;
use classes\Notebook;
use core\classes\View;
use PHPMailer\PHPMailer\PHPMailer;

require_once __DIR__ . '/vendor/autoload.php';
#spl_autoload_register('autoload');

$book = new Book('Психология влияния', 700, 540, "Роберт Чалдини");
$notebook = new Notebook('Macbook', 1500, "Apple");

$aboutInfo = [];
$aboutInfo[] = $book->getAboutInfo();
$aboutInfo[] = $notebook->getAboutInfo();

View::render('template', [
    'about' => $aboutInfo
]);


/*function autoload($class)
{
    $path = __DIR__ . '/' . str_replace("\\", '/', $class) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
}*/