<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        html {
            font-family: monospace;
        }

        h3 {
            margin-top: 0;
        }

        div {
            margin: 20px;
            border: 1px solid #999;
            padding: 20px;
            width: fit-content;
        }

        .important-line {
            border-color: red;
        }
    </style>
</head>
<body>