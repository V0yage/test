<?php

namespace core\interfaces;

interface About
{
    public function getAboutInfo();
}