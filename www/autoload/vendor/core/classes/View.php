<?php

namespace core\classes;

class View
{
    public static $templatesPath = __DIR__ . '/../../../templates/';

    public static function render($templateName, $params)
    {
        extract($params);

        ob_start();
        include_once self::$templatesPath . 'header.php';
        include_once self::$templatesPath . $templateName . '.php';
        include_once self::$templatesPath . 'footer.php';
        echo ob_get_clean();
    }
}