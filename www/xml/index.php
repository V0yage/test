<?php
/*
 * Извлечение данных из csv и формирование xml в двух вариантах:
 * 1) без создания промежуточного массива
 * 2) с созданием промежуточного массива
 *
 * Разница в производительности ~ вариант 2 в 2.5 раза быстрее
 *
 * */

#header('Content-Type: text/xml; charset=utf-8');

$csv = getCSVContent('source.csv');

/* Вариант 1 */
$xmlString = '<?xml version="1.0" encoding="utf-8"?><base></base>';

$timeStart1 = microtime(true);
$xml1 = new SimpleXMLElement($xmlString);
foreach ($csv as $item) {
    $houseId = $item['house_id'];
    $houseItem = null;
    foreach ($xml1->xpath('house') as $house) {
        if ($houseId == (string)$house->id) {
            $houseItem = $house;
            $sections = $house->sections;
            break;
        }
    }
    if (!$houseItem) {
        $houseItem = $xml1->addChild('house');
        $houseItem->addChild('id', $houseId);
        $houseItem->addChild('building', $item['building_id']);
        $sections = $houseItem->addChild('sections');
    }

    $sectionId = $item['section_id'];
    $sectionItem = null;
    foreach ($xml1->xpath('house/sections/section') as $section) {
        if ($sectionId == $section['name']) {
            $sectionItem = $section;
            break;
        }
    }
    if (!$sectionItem) {
        $cnt++;
        $sectionItem = $sections->addChild('section');
        $sectionItem->addAttribute('name', $sectionId);
    }

    $flat = $sectionItem->addChild('flat');
    $flat->addChild('id', $item['flat_id']);
    $flat->addChild('rooms', $item['rooms']);
}
$writeStatus1 = $xml1->asXML('feed1.xml');
$timeEnd1 = microtime(true);


/* Вариант 2 */
$timeStart2 = microtime(true);
$struct = [];
$cnt = 0;
foreach ($csv as $item) {
    $houseId = (int)$item['house_id'];
    if (!isset($struct[$houseId])) {
        $struct[$houseId] = [];
        $buildingId = $item['building_id'];
        $struct[$houseId]['id'] = $houseId;
        $struct[$houseId]['building'] = $buildingId;
        $struct[$houseId]['sections'] = [];
    }

    $sectionId = (int)$item['section_id'];
    if (!isset($struct[$houseId]['sections'][$sectionId])) {
        $struct[$houseId]['sections'][$sectionId] = [];
    }

    $flatId = $item['flat_id'];
    $rooms = $item['rooms'];
    $struct[$houseId]['sections'][$sectionId][] = [
        'id' => $flatId,
        'rooms' => $rooms,
    ];
}

$xml2 = new SimpleXMLElement($xmlString);
foreach ($struct as $houseItem) {
    $house = $xml2->addChild('house');
    $house->addChild('id', $houseItem['id']);
    $house->addChild('building', $houseItem['building']);
    $sections = $house->addChild('sections');

    foreach ($houseItem['sections'] as $id => $sectionItem) {
        $section = $sections->addChild('section');
        $section->addAttribute('name', $id);

        foreach ($sectionItem as $flatItem) {
            $flat = $section->addChild('flat');
            $flat->addChild('id', $flatItem['id']);
            $flat->addChild('rooms', $flatItem['rooms']);
        }
    }
}
$writeStatus2 = $xml2->asXML('feed2.xml');
$timeEnd2 = microtime(true);


/* Расчёт производительности */
if ($writeStatus1) {
    $diff1 = $timeEnd1 - $timeStart1;
    echo "success xml1 create: {$diff1}s<br>";
}
if ($writeStatus2) {
    $diff2 = $timeEnd2 - $timeStart2;
    echo "success xml2 create: {$diff2}s<br>";
}
if ($diff1 > $diff2) {
    echo 'option 2 is ' . round($diff1 / $diff2, 1) . ' as fast<br>';
} else {
    echo 'option 1 is ' . round($diff2 / $diff1, 1) . ' as fast<br>';
}


function getCSVContent($fileName) {
    $handle = fopen(__DIR__ . '/source.csv', 'r');
    $res = [];

    $headers = fgetcsv($handle);

    while ($each = fgetcsv($handle)) {
        $res[] = array_combine($headers, array_values($each));
    }

    fclose($handle);

    return $res;
}

function debug($var) {
    echo '<pre>' . print_r($var, 1) . '</pre>';
}