<?php
// сюда нужно вписать токен вашего бота
define('TELEGRAM_TOKEN', '1137810553:AAH5E7irI6MWVN9-ZZk8QHzul1ujqk-L9l8');

// сюда нужно вписать ваш внутренний айдишник
define('TELEGRAM_CHATID', '329877264');

message_to_telegram('Привет!');

function message_to_telegram($text)
{
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            #CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/getUpdates',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            /*CURLOPT_POSTFIELDS => array(
                'chat_id' => TELEGRAM_CHATID,
                'text' => $text,
            ),*/
        )
    );
    $res = curl_exec($ch);
    return json_decode($res);
    #return $res;
}

function get_bot_info() {
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/getMe',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
        )
    );
    $res = curl_exec($ch);
    #return json_decode($res);
    return $res;
}

function send_location() {
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendLocation',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => TELEGRAM_CHATID,
                'latitude' => 30,
                'longitude' => 60,
            ),
        )
    );
    $res = curl_exec($ch);
}

function get_bot_commands() {
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/getMyCommands',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
        )
    );
    $res = curl_exec($ch);
    return $res;
}

function set_bot_commands($commands) {
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/setMyCommands',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'commands' => json_encode($commands)
            ),
        )
    );
    $res = curl_exec($ch);
    var_dump($res);
}