<?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/pagination/general.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . '/pagination/pagination.php';

    $perpage = 3;
    if (!$_GET['page'] || !ctype_digit($_GET['page']) || !((int)$_GET['page'])) {
        $activePage = 1;
    } else {
        $activePage = $_GET['page'];
    }

    $offset = ($activePage - 1) * $perpage;

    $query = "SELECT * FROM posts LIMIT $perpage OFFSET $offset";
    $res = mysqli_query($dbConnect, $query);
    $rows = mysqli_fetch_all($res, MYSQLI_ASSOC);
    debug($rows);


    $query = 'SELECT COUNT(*) FROM posts';
    $res = mysqli_query($dbConnect, $query);
    $rowsNum = mysqli_fetch_row($res)[0];

    $lastPage = ceil($rowsNum / $perpage);

    $pagesLimit = min(5, $lastPage);
    $shownPages = array();
    $prevPage = (int)$activePage;
    $nextPage = (int)$activePage;
    $cnt = 0;

    while (1) {
        if (!(count($shownPages) < $pagesLimit)) {
            break;
        }
        if ($prevPage > 0 && !in_array($prevPage, $shownPages)) {
            array_unshift($shownPages, $prevPage);
        }
        $prevPage--;

        if (!(count($shownPages) < $pagesLimit)) {
            break;
        }
        if ($nextPage <= $lastPage && !in_array($nextPage, $shownPages)) {
            array_push($shownPages, $nextPage);
        }
        $nextPage++;
    }
?>

    <ul class="pagination">
    <?php if ($activePage == 1): ?>
        <li class="pagination__item" disabled>
            <span class="pagination__link">Start</span>
        </li>
    <?php else: ?>
        <li class="pagination__item">
            <a class="pagination__link" href="./">Start</a>
        </li>
    <?php endif; ?>

    <?php foreach ($shownPages as $shownPage): ?>
        <?php $activeClass = $activePage == $shownPage ? ' active' : ''; ?>
        <li class="pagination__item<?= $activeClass ?>">
            <?php if ($activeClass): ?>
                <span class="pagination__link"><?= $shownPage ?></span>
            <?php else: ?>
                <a class="pagination__link" href="./?page=<?= $shownPage ?>"><?= $shownPage ?></a>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>

    <?php if ($activePage == $lastPage): ?>
        <li class="pagination__item" disabled>
            <span class="pagination__link">End</span>
        </li>
    <?php else: ?>
        <li class="pagination__item">
            <a class="pagination__link" href="./?page=<?= $lastPage ?>">End</a>
        </li>
    <?php endif; ?>
    </ul>
<?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/pagination/template.php';

    /*pagination(array(
        'pagesLimit' => 1,
    ));*/