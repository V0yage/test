<?php
    $debug = include_once $_SERVER['DOCUMENT_ROOT'] . '/pagination/funcs.php';
    $config = require_once $_SERVER['DOCUMENT_ROOT'] . '/pagination/config.php';
    $dbParams = $config['connections'];

    $dbConnect = mysqli_connect(
        $dbParams['host'],
        $dbParams['user'],
        $dbParams['password'],
        $dbParams['database']
    );
    if (!$dbConnect) {
        die(mysqli_connect_error());
    }