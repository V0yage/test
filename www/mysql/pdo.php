<?php

$host = 'localhost';
$db = 'world';
$user = 'root';
$pass = 'root';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
];

$pdo = new PDO($dsn, $user, $pass, $opt);
/*$stmt = $pdo->query('SELECT Code, Name FROM country');
while ($each = $stmt->fetch()) {
    echo "Code: {$each['Code']}; Name: {$each['Name']}<br>";
}*/
$stmt = $pdo->prepare('SELECT CountryCode, Name FROM city WHERE CountryCode = :CountryCode');
$stmt->execute([':CountryCode' => 'AFG']);
while ($each = $stmt->fetch(PDO::FETCH_LAZY)) {
    echo "Code: {$each->CountryCode}; Name: {$each->Name}<br>";
}

//foreach ($stmt as $each) {
//    echo "Code: {$each['CountryCode']}; Name: {$each['Name']}<br>";
//}

