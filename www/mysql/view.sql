
CREATE PROCEDURE create_view()
BEGIN
    CREATE VIEW country_view AS
    SELECT DISTINCT continent, COUNT(*) AS countries_num FROM country GROUP BY continent;
END
