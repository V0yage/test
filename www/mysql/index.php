<?php

$mysql = new mysqli('localhost', 'root', 'root', 'world');
if ($mysql->connect_errno) {
    echo $mysql->connect_error;
    exit;
}

if (isset($_GET['view'])) {
    $query = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/mysql/view.sql');
    $res = $mysql->query($query);
    if ($mysql->errno) {
        echo $mysql->error;
        exit;
    }

    $query = 'CALL create_view();';
    $res = $mysql->query($query);
    if ($mysql->errno) {
        echo $mysql->error;
        exit;
    }
}

$query = "SELECT * FROM country_view WHERE continent LIKE 'A%'";
$res = $mysql->query($query);
if ($mysql->errno) {
    echo $mysql->error;
    exit;
}
$items = $res->fetch_all();
debug($items);

function debug($vars) {
    echo '<pre>' . print_r($vars, 1) . '</pre>';
}
