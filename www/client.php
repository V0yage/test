<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <? if ($_GET): ?>
        <pre>
            <? print_r($_GET); ?>
        </pre>
    <? else: ?>
        <form action="">
            <? for ($i = 0; $i < 10; $i++): ?>
                <label><input type="checkbox" name="items[]" value="<?= $i ?>">&nbsp;<?= $i ?></label><br>
            <? endfor; ?>
            <input type="submit" value="Send">
        </form>
    <? endif; ?>
    <br>
    <button id="check-odd">Check odd items</button>&nbsp;
    <button id="check-even">Check even items</button>
    <script>
        var oddBtn = document.querySelector('#check-odd');
        var evenBtn = document.querySelector('#check-even');
        var checkItems = document.querySelectorAll('[type="checkbox"]');

        [oddBtn, evenBtn].forEach(function(btn) {
            btn.addEventListener('click', function() {
                console.log(this);
                [...checkItems].forEach((item, key) => {
                    if (this.id === 'check-odd' && !(key % 2) ||
                        this.id === 'check-even' && key % 2) {
                        item.checked = true;
                    } else {
                        item.checked = false;
                    }
                });
            });
        });
    </script>
</body>
</html>