<?php
    header('Content-type: text/xml; charset=utf-8');

    $pathToDir = $_SERVER['DOCUMENT_ROOT'] . '/xml_creator';
    $xmlFileName = 'feed.xml';
    $pathToXml = $pathToDir . DIRECTORY_SEPARATOR . $xmlFileName;

    $xmlSrc = '<?xml version="1.0" encoding="utf-8"?><base></base>';
    $xml = new SimpleXMLElement($xmlSrc);
    $xml->addChild('type', 'Трёшка');
    $xml->asXML($pathToXml);

    echo $xml->asXML();