<?php

$input = <<<EOF
1;PHP;Любит знаки доллара
2;Python;Любит пробелы
3;Ruby;Любит блоки
EOF;

foreach (parseInput($input) as $id => $fields) {
    echo "$id:<br>&nbsp;$fields[0]<br>&nbsp;$fields[1]<br><br>";
}


echo '<br>';
foreach (xrange(1, 9, 2) as $item) {
    echo "$item ";
}

function parseInput($input) {
    foreach (explode("\n", $input) as $item) {
        $fields = explode(';', $item);
        $id = array_shift($fields);
        yield $id => $fields;
    }
}

function xrange($start, $limit, $step) {
    if ($start <= $limit) {
        if ($step <= 0) {
            throw new LogicException('Шаг должен быть положительным');
        }
        for ($c = $start; $c <= $limit; $c += $step) {
            yield $c;
        }
    } else {
        if ($step >= 0) {
            throw new LogicException('Шаг должен быть отрицательным');
        }
        for ($c = $limit; $c >= $start; $c -= $step) {
            yield $c;
        }
    }
}